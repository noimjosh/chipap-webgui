#!/bin/bash
clear
echo Password is required for sudo operations
echo
echo Getting updated GUI files
cd /var/www/html
sudo git reset --hard master
sudo git pull origin
cd ~
echo
echo Updating permissions
sudo chown -R www-data:www-data /var/www/html
echo
echo Moving required files
sudo mv /var/www/html/battery.sh /usr/local/bin/battery.sh
sudo chmod +x /usr/local/bin/battery.sh
echo
echo Removing unnecessary files
sudo rm /var/www/html/raspap.php
sudo rm /var/www/html/update-gui.sh
echo
echo Done!
