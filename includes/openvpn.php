<?php
/**
*
*
*/
function DisplayOpenVPNConfig() {
	if( isset($_POST['SaveOpenVPNSettings']) && isset($_POST['VPNconfig']) ) {
		echo $_POST['VPNconfig']." set as the default configuration file";
	  file_put_contents("/var/www/html/includes/vpn.default", $_POST['VPNconfig']);
	} elseif( isset($_POST['StartOpenVPN']) && isset($_POST['VPNconfig']) ) {
		$ConfigFile = substr($_POST['VPNconfig'], 0, -5);
		echo "Attempting to connect via OpenVPN to ".$ConfigFile;
		// write current vpn to id file for later recall
		file_put_contents("/var/www/html/includes/vpn.id", $ConfigFile);
	  exec( 'sudo /bin/systemctl start openvpn@'.$ConfigFile.'.service', $return );
	  foreach( $return as $line ) {
	    echo $line."<br />";
	  }
	} elseif( isset($_POST['StopOpenVPN']) ) {
	  echo "Attempting to stop openvpn";
		// Get the current VPN configuration file
		$ConfigFile = file_get_contents("/var/www/html/includes/vpn.id");
		// Delete the vpn.id file
		unlink("/var/www/html/includes/vpn.id");
	  exec( 'sudo /bin/systemctl stop openvpn@'.$ConfigFile.'.service', $return );
	  foreach( $return as $line ) {
	    echo $line."<br />";
	  }
	}

	//exec( 'cat '. RASPI_OPENVPN_CLIENT_CONFIG, $returnClient );
	//exec( 'cat '. RASPI_OPENVPN_SERVER_CONFIG, $returnServer );
	exec( 'pidof openvpn | wc -l', $openvpnstatus);

	if( $openvpnstatus[0] == 0 ) {
		$status = '<div class="alert alert-warning alert-dismissable">OpenVPN is not running
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>';
	} else {
		$status = '<div class="alert alert-success alert-dismissable">OpenVPN is running: ';
		$status .= file_get_contents("/var/www/html/includes/vpn.id");
		$status .='<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>';
	}

	// Remove stored VPN file if no longer required
	if( $openvpnstatus[0] == 0 && file_exists("/var/www/html/includes/vpn.id")) {
		unlink("/var/www/html/includes/vpn.id");
	}

// The lines below are original from the raspi-webgui files, not required here
/*
	// parse client settings
	foreach( $returnClient as $a ) {
		if( $a[0] != "#" ) {
			$arrLine = explode( " ",$a) ;
			$arrClientConfig[$arrLine[0]]=$arrLine[1];
		}
	}

	// parse server settings
	foreach( $returnServer as $a ) {
		if( $a[0] != "#" ) {
			$arrLine = explode( " ",$a) ;
			$arrServerConfig[$arrLine[0]]=$arrLine[1];
		}
	} */
	?>
	<div class="row">
	<div class="col-lg-12">
    	<div class="panel panel-primary">
			<div class="panel-heading"><i class="fa fa-lock fa-fw"></i> Configure OpenVPN
            </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
        	<!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#openvpnclient" data-toggle="tab">Client settings</a>
                </li>
                <li><a href="#openvpnserver" data-toggle="tab">Server settings</a>
                </li>
            </ul>
            <!-- Tab panes -->
           	<div class="tab-content">
           		<p><?php echo $status; ?></p>
            	<div class="tab-pane fade in active" id="openvpnclient">

            		<h4>Select Client Configuration</h4>
					<form role="form" action="?page=openvpn_conf" method="POST">
					<div class="row">
						<div class="form-group col-md-4">
							<?php
								echo "<select name='VPNconfig'";
								if (file_exists("/var/www/html/includes/vpn.id")) {
									echo " disabled";
								}
								echo ">";
								$dir = '/etc/openvpn/';
								$files = scandir($dir);
								sort($files);
								foreach ($files as $file) {
								    if ($file != '.' && $file != '..' && strtolower(substr($file, strrpos($file, '.') + 1)) == 'conf') {
											echo "<option value='$file'";
											if (file_exists("/var/www/html/includes/vpn.id")) {
												if (file_get_contents("/var/www/html/includes/vpn.id") == substr($file, 0, -5)) {
													echo " selected";
												}
											} elseif (file_exists("/var/www/html/includes/vpn.default")) {
												if ($file == file_get_contents("/var/www/html/includes/vpn.default")) {	// set default config file here
													echo " selected";
												}
											}
											echo ">$file</option>";
								    }
								}
/*
								if ($handle = opendir('/etc/openvpn/')) {
										while (false !== ($file = readdir($handle)))
										{
												if ($file != "." && $file != ".." && strtolower(substr($file, strrpos($file, '.') + 1)) == 'conf')
												{
														echo "<option value='$file'";
														if (file_exists("/var/www/html/includes/vpn.id")) {
															if (file_get_contents("/var/www/html/includes/vpn.id") == substr($file, 0, -5)) {
																echo " selected";
															}
														} elseif (file_exists("/var/www/html/includes/vpn.default")) {
															if ($file == file_get_contents("/var/www/html/includes/vpn.default")) {	// set default config file here
																echo " selected";
															}
														}
														echo ">$file</option>";
												}
										}
										closedir($handle);
									}
*/
							?>
							</select>
	          </div>
					</div>
				</div>
				<div class="tab-pane fade" id="openvpnserver">
            		<h4>Server settings</h4>
            		<div class="row">
						<div class="form-group col-md-4">
            			<label for="code">Port</label>
            			<input type="text" class="form-control" name="openvpn_port" value="<?php echo $arrServerConfig['port'] ?>" />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-4">
						<label for="code">Protocol</label>
						<input type="text" class="form-control" name="openvpn_proto" value="<?php echo $arrServerConfig['proto'] ?>" />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-4">
						<label for="code">Root CA certificate</label>
						<input type="text" class="form-control" name="openvpn_rootca" placeholder="<?php echo $arrServerConfig['ca']; ?>" disabled />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-4">
						<label for="code">Server certificate</label>
						<input type="text" class="form-control" name="openvpn_cert" placeholder="<?php echo $arrServerConfig['cert']; ?>" disabled />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-4">
						<label for="code">Diffie Hellman parameters</label>
						<input type="text" class="form-control" name="openvpn_dh" placeholder="<?php echo $arrServerConfig['dh']; ?>" disabled />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-4">
						<label for="code">KeepAlive</label>
						<input type="text" class="form-control" name="openvpn_keepalive" value="<?php echo $arrServerConfig['keepalive']; ?>" />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-4">
						<label for="code">Server log</label>
						<input type="text" class="form-control" name="openvpn_status" placeholder="<?php echo $arrServerConfig['status']; ?>" disabled />
						</div>
					</div>
            	</div>
				<input type="submit" class="btn btn-outline btn-primary" name="SaveOpenVPNSettings" value="Save settings" />
				<?php
				if($openvpnstatus[0] == 0) {
					echo '<input type="submit" class="btn btn-success" name="StartOpenVPN" value="Start OpenVPN" />';
				} else {
					echo '<input type="submit" class="btn btn-warning" name="StopOpenVPN" value="Stop OpenVPN" />';
				}
				?>
				</form>
		</div><!-- /.panel-body -->
    </div><!-- /.panel-primary -->
    <div class="panel-footer"> Information provided by openvpn</div>
</div><!-- /.col-lg-12 -->
</div><!-- /.row -->
<?php
}


?>
