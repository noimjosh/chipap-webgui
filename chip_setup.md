# Create a headless pass-thru C.H.I.P.

* Flash new Headless 4.4 firmware
* Open TTY to CHIP
* Change "chip" password
  * `passwd`
  * `sudo passwd root`
* Uninstall network-manager
  * `sudo apt-get remove network-manager`
* Switch wpa_supplicant to desired install
  * `sudo apt-get install wpasupplicant`
* Add the following to /etc/network/interfaces

```
auto wlan0
iface wlan0 inet dhcp
    wpa-driver nl80211
    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
```

* Create /etc/wpa_supplicant/wpa_supplicant.conf and add code below
  * `sudo vi /etc/wpa_supplicant/wpa_supplicant.conf`

```
network={
    ssid="YOUR_SSID"
    psk="yourP@55W0RD"
    priority=9
}
```

* Start wlan0
  * `sudo ifup wlan0`
* Verify wlan0 connected to your WiFi
  * `sudo ifconfig`
* Perform aptitude update & upgrade to ensure all packages are newest
  * `sudo apt-get update | sudo apt-get upgrade`

* Install tmux
  * `sudo apt-get install tmux`

## Create Access Point
Derived from https://slack-files.com/T02GVC9G6-F0H7G3WCT-25e7dfb781
* Install dnsmasq
  * `sudo apt-get install dnsmasq`
* configure dnsmasq (add lines to end of file)
  * `sudo vi /etc/dnsmasq.d/access_point.conf`

```
#If you want dnsmasq to listen for DHCP and DNS requests only on
#specified interfaces (and the loopback) give the name of the
#interface (eg eth0) here.
#Repeat the line for more than one interface.
interface=wlan1
#Or you can specify which interface not to listen on
except-interface=wlan0

#Uncomment this to enable the integrated DHCP server, you need
#to supply the range of addresses available for lease and optionally
#a lease time. If you have more than one network, you will need to
#repeat this for each network on which you want to supply DHCP
#service.
dhcp-range=172.20.0.100,172.20.0.250,1h
```

* Establish static IP for C.H.I.P. AP (add to the file just before the line "auto wlan0")
  * `sudo vi /etc/network/interfaces`
  * Side note - adding before wlan0 ensures that the local host is established without a known WiFi connection.

```
auto wlan1

iface wlan1 inet static
    address 172.20.0.1
    netmask 255.255.255.0
```

* Verify it worked
  * `sudo ifup wlan1`
  * `ip addr show wlan1`
* Restart DHCP server
  * `sudo systemctl restart dnsmasq`
* Configure the Access Point (add code to file)
  * `sudo vi /etc/hostapd.conf`

```
interface=wlan1
driver=nl80211
ssid=your_ssid
channel=1
ctrl_interface=/var/run/hostapd
auth_algs=3
max_num_sta=10
wpa=2
wpa_passphrase=your passphrase
wpa_pairwise=TKIP CCMP
rsn_pairwise=CCMP
```

* Establish hostapd service
  * `sudo vi /lib/systemd/system/hostapd-systemd.service`

```
[Unit]
Description=hostapd service
Wants=network-manager.service
After=network-manager.service
Wants=module-init-tools.service
After=module-init-tools.service
ConditionPathExists=/etc/hostapd.conf

[Service]
ExecStart=/usr/sbin/hostapd /etc/hostapd.conf

[Install]
WantedBy=multi-user.target
```

* Disable existing systemV script for hostapd
  * `sudo update-rc.d hostapd disable`
* Setup systemd service for hostapd
  * `sudo systemctl daemon-reload`
  * `sudo systemctl enable hostapd-systemd`
* Reboot CHIP
  * `sudo reboot`

## Set up VPN
* Install OpenVPN
  * `sudo apt-get install openvpn`
* Copy ovpn file to /etc/openvpn/client.conf
* Copy auth.txt file to /etc/openvpn/auth.txt


## Set Up Forwarding
* Establish iptables (copy below to file)
  * `sudo mkdir /etc/iptables`
  * `sudo vi /etc/iptables/rules.v4`

```
*nat
:PREROUTING ACCEPT [227:70609]
:INPUT ACCEPT [8:943]
:OUTPUT ACCEPT [10:792]
:POSTROUTING ACCEPT [0:0]
-A POSTROUTING -o wlan0 -j MASQUERADE

# Used for OpenVPN NAT
-A POSTROUTING -o tun0 -j MASQUERADE
COMMIT
*filter
:INPUT ACCEPT [122:14689]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [43:3812]

# Used to block incoming wlan0 requests
-A INPUT -i wlan0 -j DROP
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT

# Used to allow SSH
-A INPUT -i wlan0 -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
-A INPUT -i wlan1 -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT

# Used for regular forwarding
-A FORWARD -i wlan0 -o wlan1 -m state --state RELATED,ESTABLISHED -j ACCEPT
-A FORWARD -i wlan1 -o wlan0 -j ACCEPT

# Used for OpenVPN forwarding
-A FORWARD -i tun0 -o wlan1 -m state --state RELATED,ESTABLISHED -j ACCEPT
-A FORWARD -i wlan1 -o tun0 -j ACCEPT
COMMIT
```

* Restore the newly created rules (assuming you have no others)
  * `sudo iptables-restore /etc/iptables/rules.v4`
* Enable newly created rules at boot
  * `sudo vi /etc/rc.local`

```
iptables-restore /etc/iptables/rules.v4
```

* Enable NAT at boot
  * `sudo vi /etc/sysctl.conf`

```
net.ipv4.ip_forward=1
```
* Enable NAT now
  * `sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"`

* Reboot CHIP
  * `sudo reboot`

## Install CHIPAP-WEBGUI
* Install git
  * `$ sudo apt-get install git`
* Install required dependencies for chipap-webgui
  * `sudo apt-get install lighttpd php5-cgi`
* Enable fastcgi-php module
  * `sudo lighty-enable-mod fastcgi-php`
  * `sudo service lighttpd restart`
* Update sudoers file for chipap-webgui (add below to bottom of file)
  * `$ sudo visudo`

```
www-data ALL=(ALL) NOPASSWD:/sbin/ifdown wlan0
www-data ALL=(ALL) NOPASSWD:/sbin/ifup wlan0
www-data ALL=(ALL) NOPASSWD:/bin/cat /etc/wpa_supplicant/wpa_supplicant.conf
www-data ALL=(ALL) NOPASSWD:/bin/cp /tmp/wifidata /etc/wpa_supplicant/wpa_supplicant.conf
www-data ALL=(ALL) NOPASSWD:/sbin/wpa_cli scan_results
www-data ALL=(ALL) NOPASSWD:/sbin/wpa_cli scan
www-data ALL=(ALL) NOPASSWD:/sbin/wpa_cli reconfigure
www-data ALL=(ALL) NOPASSWD:/bin/cp /tmp/hostapddata /etc/hostapd.conf
www-data ALL=(ALL) NOPASSWD:/bin/systemctl start hostapd-systemd
www-data ALL=(ALL) NOPASSWD:/bin/systemctl stop hostapd-systemd
www-data ALL=(ALL) NOPASSWD:/bin/systemctl start dnsmasq
www-data ALL=(ALL) NOPASSWD:/bin/systemctl stop dnsmasq
www-data ALL=(ALL) NOPASSWD:/bin/cp /tmp/dhcpddata /etc/dnsmasq.d/access_point.conf
www-data ALL=(ALL) NOPASSWD:/sbin/shutdown -h now
www-data ALL=(ALL) NOPASSWD:/sbin/reboot
www-data ALL=(ALL) NOPASSWD:/usr/local/bin/battery.sh
www-data ALL=(ALL) NOPASSWD:/bin/systemctl start openvpn*
www-data ALL=(ALL) NOPASSWD:/bin/systemctl stop openvpn*
www-data ALL=(ALL) NOPASSWD:/bin/systemctl start smbd
www-data ALL=(ALL) NOPASSWD:/bin/systemctl stop smbd
```
* Clone chipap-webgui into lighttpd directory
  * `sudo rm -rf /var/www/html`
  * `sudo git clone https://gitlab.com/noimjosh/chipap-webgui /var/www/html`
* Change file ownership to www-data
  * `sudo chown -R www-data:www-data /var/www/html`
* Move config file to proper location & modify ownership
  * `sudo mkdir /etc/raspap`
  * `sudo mv /var/www/html/raspap.php /etc/raspap/raspap.php`
  * `sudo chown -R www-data:www-data /etc/raspap`
* Move battery.sh file to proper location & allow execution
  * `sudo mv /var/www/html/battery.sh /usr/local/bin/battery.sh`
  * `sudo chmod +x /usr/local/bin/battery.sh`
* The default username is 'admin' and the default password is 'secret'.
* Copy update-gui.sh to proper location & allow execution
  * `sudo mv /var/www/html/update-gui.sh /usr/local/bin/update-gui.sh`
  * `sudo chmod +x /usr/local/bin/update-gui.sh`
* Install wireless-tools to display wifi data Web GUI dashboard
  * `sudo apt-get install wireless-tools`
* Restart CHIP
  * `sudo reboot`
